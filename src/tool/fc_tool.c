/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 * This file contains some functions for parsing the FAST_CJSON to Given
 * Data structure
 *
 * There are 4 pair macros to do this job:
 *
 * 1. CJSON_FIELD_DEF(name)
 *    CJSON_FIELD_DEF_END(name)
 *    This macro will generate the structure named: CJSON_name, so if you given the `nam`e with 'data',
 *    the data will generate to CJSON_data
 *
 * 2. CJSON_FIELD_FUNC_DEF(name)
 *    This macro used to generate the function declarations
 *
 * 3. CJSON_FIELD_FUNC(name)
 *    CJSON_FIELD_FUNC_END()
 *
 * 4. CJSON_FIELD_CMP(name, len, if/elif)
 */

#include <fc_tool.h>
#include <fc_string.h>
